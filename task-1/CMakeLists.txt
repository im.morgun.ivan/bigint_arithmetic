cmake_minimum_required(VERSION 3.20)
project(task_1)

set(CMAKE_CXX_STANDARD 17)

set(PROJECT_NAME GameOfLife)
add_subdirectory(game)
add_subdirectory(tests)
