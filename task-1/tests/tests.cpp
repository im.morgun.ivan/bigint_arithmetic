//
// Created by mimor on 12-Dec-22.
//
#include "gtest/gtest.h"
#include "CellField.hpp"
#include "DataStorage.hpp"
#include "LogicController.hpp"


TEST(LogicControllerTest, CellDie) {
    DataStorage dataStorage;
    dataStorage.setCellsForBirth({3});
    dataStorage.setCellsForSurvival({2, 3});
    LogicController logicController = LogicController(3, 3, {{1, 1}});
    logicController.calculateField(dataStorage, 1);

    EXPECT_FALSE(logicController.getCurrentField().getCell(1, 1).getAlive());
}

TEST(LogicControllerTest, CellBirthSurvive) {
    DataStorage dataStorage;
    dataStorage.setCellsForBirth({3});
    dataStorage.setCellsForSurvival({2, 3});
    LogicController logicController = LogicController(5, 5, {{1, 2}, {2, 2}, {3, 2}});
    logicController.calculateField(dataStorage, 1);

    EXPECT_FALSE(logicController.getCurrentField().getCell(1, 2).getAlive());
    EXPECT_FALSE(logicController.getCurrentField().getCell(3, 2).getAlive());
    EXPECT_TRUE(logicController.getCurrentField().getCell(2, 1).getAlive());
    EXPECT_TRUE(logicController.getCurrentField().getCell(2, 2).getAlive());
    EXPECT_TRUE(logicController.getCurrentField().getCell(2, 3).getAlive());
}

TEST(LogicControllerTest, ToroidalField) {
    DataStorage dataStorage;
    dataStorage.setCellsForBirth({3});
    dataStorage.setCellsForSurvival({2, 3});
    LogicController logicController = LogicController(3, 3, {{0, 1}, {1, 1}, {2, 1}});
    logicController.calculateField(dataStorage, 1);

    EXPECT_TRUE(logicController.getCurrentField().getCell(0, 1).getAlive());
    EXPECT_TRUE(logicController.getCurrentField().getCell(1, 1).getAlive());
    EXPECT_TRUE(logicController.getCurrentField().getCell(2, 1).getAlive());
}
