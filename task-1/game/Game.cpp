//
// Created by mimor on 16-Oct-22.
//

#include <iostream>
#include <regex>
#include <CustomException.hpp>
#include <DataStorage.hpp>
#include <LogicController.hpp>
#include <View.hpp>
#include <thread>

enum Command {
    TICK,
    DUMP,
    HELP,
    EXIT,
    UNKNOWN,
};

typedef struct command_t {
    Command command;
    int intValue;
    std::string stringValue;
} command_t;

command_t parseCommand(const std::string&);

void onlineMode(const DataStorage&, const View&, LogicController&);

int main(int argc, char* argv[]) {
    CommandLineArgs cmdLArgs;
    try {
        cmdLArgs = Utils::ParseCmdLArgs(argc, argv);
    } catch (const CustomException& exception) {
        std::cerr << exception.what() << std::endl;
        return 1;
    }

    DataStorage dataStorage;
    try {
        dataStorage = Utils::ParseInput(cmdLArgs.getInputFile());
    } catch (const CustomException& exception) {
        std::cerr << exception.what() << std::endl;
        return 1;
    }

    dataStorage.setWidth(10);
    dataStorage.setHeight(10);

    LogicController logicController = LogicController(
            dataStorage.getWidth(),
            dataStorage.getHeight(),
            dataStorage.getStartAliveCells());
    logicController.calculateField(dataStorage, cmdLArgs.getIterations());
    View view = View(dataStorage);
    if (cmdLArgs.getOutputFile().empty())
        onlineMode(dataStorage, view, logicController);
    else
        view.save(cmdLArgs.getOutputFile(), logicController.getCurrentField());
}

void onlineMode(const DataStorage& dataStorage, const View& view, LogicController& logicController) {
    view.display(logicController.getCurrentField());
    std::string commandString;
    while (true) {
        std::getline(std::cin, commandString);
        command_t command = parseCommand(commandString);
        switch (command.command) {
            case HELP: {
                view.printHelp();
                break;
            }
            case DUMP: {
                view.save(command.stringValue, logicController.getCurrentField());
                break;
            }
            case TICK: {
                for (int i = 0; i < command.intValue; i++) {
                    logicController.calculateField(dataStorage, 1);
                    view.display(logicController.getCurrentField());
                    std::this_thread::sleep_for(std::chrono::milliseconds(300));
                }
                break;
            }
            case EXIT: {
                return;
            }
            default:
                std::cout << "Unknown command, try to use help" << std::endl;
        }
    }
}

command_t parseCommand(const std::string& command) {
    if (command == "help") {
        return {HELP, 0, ""};
    }
    if (command == "exit") {
        return {EXIT, 0, ""};
    }
    if (std::regex_match(command, std::regex(R"(tick\s\d{1,30}\s{0,})", std::regex_constants::ECMAScript))) {
        return {TICK, std::stoi(command.substr(5)), ""};
    }
    if (std::regex_match(command, std::regex(R"(dump\s.+\s{0,})", std::regex_constants::ECMAScript))) {
        return {DUMP, 0, command.substr(5)};
    }
    return {UNKNOWN, 0, ""};
}
