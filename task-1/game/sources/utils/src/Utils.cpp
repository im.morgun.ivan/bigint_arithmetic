//
// Created by mimor on 18-Oct-22.
//

#include <map>
#include <fstream>
#include <regex>
#include <iostream>
#include "Utils.hpp"
#include "CustomException.hpp"

CommandLineArgs Utils::ParseCmdLArgs(int argc, char* argv[]) {
    const char* short_options = "hi:I:o:";
    const struct option long_options[] = {
            {
                .name="iterations", .has_arg=required_argument, .flag=NULL, .val='i',
            },
            {
                .name="input", .has_arg=required_argument, .flag=NULL, .val='I',
            },
            {
                .name="output", .has_arg=required_argument, .flag=NULL, .val='o',
            },
    };

    CommandLineArgs cmdLArgs;
    char parameter;
    int option_number;
    while ((parameter = (char) getopt_long(argc, argv, short_options, long_options, &option_number)) != -1) {
        switch (parameter) {
            case 'i': {
                cmdLArgs.setIterations(std::stoi(optarg));
                break;
            }
            case 'I': {
                cmdLArgs.setInputFile(optarg);
                break;
            }
            case 'o': {
                cmdLArgs.setOutputFile(optarg);
                break;
            }
            default:
                throw CustomException("Unknown command line argument!");
        }
    }

    return cmdLArgs;
}

int Utils::CountAliveNeighbours(int x, int y, const CellField& field) {
    int aliveNumber = 0;
    for (int shiftX = -1; shiftX <= 1; shiftX++) {
        for (int shiftY = -1; shiftY <= 1; shiftY++) {
            if (shiftX == 0 && shiftY == 0)
                continue;
            int neighbourX = (x + shiftX + field.getWidth()) % field.getWidth();
            int neighbourY = (y + shiftY + field.getHeight()) % field.getHeight();
            if (field.getCell(neighbourX, neighbourY).getAlive())
                aliveNumber++;
        }
    }
    return aliveNumber;
}

DataStorage Utils::ParseInput(std::string filename) {
    DataStorage dataStorage;
    std::string currentLine;
    std::ifstream file;
    file.open("./assets/" + filename);
    if (!file.is_open())
        throw CustomException("Can't open input file");
    std::vector<std::pair<std::regex, std::string>> validators = {
            { std::regex(R"(#Life 1.06\s{0,})", std::regex_constants::ECMAScript), "Invalid header line in input file" },
            { std::regex(R"(#N .+\s{0,})", std::regex_constants::ECMAScript), "Invalid universe name" },
            { std::regex(R"(#R B[0-8]{1,9}\/S[0-8]{1,9}\s{0,})", std::regex_constants::ECMAScript), "Invalid rules" },
    };
    std::vector<std::string> lines;
    for (auto validator : validators) {
        std::getline(file, currentLine);
        if (file.eof() || !std::regex_match(currentLine, validator.first)) {
            std::cout << currentLine;
            throw CustomException(validator.second);
        }

        lines.push_back(currentLine);
    }

    std::cout << lines[1].substr(3);
    dataStorage.setUniverseName(lines[1].substr(3));
    std::vector<int> birthRule, surviveRule;
    bool leftPart = true;
    for (const auto c : lines[2]) {
        int v = c - '0';
        if (c == '/')
            leftPart = false;
        if (v <= 8 && v >= 0) {
            if (leftPart)
                birthRule.push_back(v);
            else
                surviveRule.push_back(v);
        }
    }
    dataStorage.setCellsForBirth(birthRule);
    dataStorage.setCellsForSurvival(surviveRule);

    while (std::getline(file, currentLine)) {
        if (!std::regex_match(currentLine, std::regex(R"(\d+\s\d+\s{0,})")))
            throw CustomException("Invalid start alive cell");
        int x = std::stoi(currentLine);
        int y = std::stoi(currentLine.substr(currentLine.find(" ")));
        dataStorage.addStartAliveCell({x, y});
    }

    return dataStorage;
}
