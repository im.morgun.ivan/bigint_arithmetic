//
// Created by mimor on 18-Oct-22.
//

#ifndef TASK_1_UTILS_HPP
#define TASK_1_UTILS_HPP

#include "CommandLineArgs.hpp"
#include <getopt.h>
#include <Cell.hpp>
#include <vector>
#include <map>
#include <CellField.hpp>
#include <DataStorage.hpp>

class Utils {
public:
    static CommandLineArgs ParseCmdLArgs(int argc, char* argv[]);
    static DataStorage ParseInput(std::string filename);
    static int CountAliveNeighbours(int x, int y, const CellField& field);
    template<typename K, typename V>
    static V GetMapValueWithDefault(const std::map<K, V> &m, const K &key, const V &defaultValue) {
        typename std::map<K,V>::const_iterator it = m.find(key);
        if (it == m.end())
            return defaultValue;
        else
            return it->second;
    }
};


#endif //TASK_1_UTILS_HPP
