//
// Created by mimor on 13-Nov-22.
//

#include "../include/View.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <DataStorage.hpp>
#include <Utils.hpp>

void View::writeStream(std::ostream& os, const CellField &field) const {
    std::string fieldView;
    for (int x = 0; x < field.getWidth(); x++) {
        for (int y = 0; y < field.getHeight(); y++) {
            if (field.getCell(x, y).getAlive())
                fieldView += this->aliveCellSymbol;
            else
                fieldView += this->deadCellSymbol;
        }
        fieldView += "\n";
    }
    os << title << std::endl << fieldView;
}

void View::generateTitle(DataStorage& dataStorage) {
    if (this->title.empty()) {
        this->title = dataStorage.getUniverseName() + " universe\n";
        std::string birthCellsLine = "Cells to birth:";
        std::string survivalCellsLine = "Cells to survive:";
        for (int i = 0; i <= 8; i++) {
            if (Utils::GetMapValueWithDefault<int, bool>(dataStorage.getCellsForBirth(), i, false))
                birthCellsLine += " " + std::to_string(i);
            if (Utils::GetMapValueWithDefault<int, bool>(dataStorage.getCellsForSurvival(), i, false))
                survivalCellsLine += " " + std::to_string(i);
        }
        this->title += birthCellsLine + "\n" + survivalCellsLine + "\n";
        this->title.erase(std::remove(this->title.begin(), this->title.end(), '\r'), this->title.end());
        std::cout << this->title;
    }
}

View::View(DataStorage& dataStorage) {
    this->generateTitle(dataStorage);
}

void View::save(const std::string& filename, const CellField& field) const {
    std::ofstream file;
    file.open("./assets/" + filename);
    this->writeStream(file, field);
    file.close();
}

void View::display(const CellField &field) const {
    system("clear");
    this->writeStream(std::cout, field);
}

void View::printHelp() const {
    std::cout << "__________________________________________________________________________\n"
                 "| help - this message                                                    |\n"
                 "| dump <filename> - save current state in file                           |\n"
                 "| tick <n> - number of ticks to be counted from current state (0<n<2^30) |\n"
                 "| exit - exit the game                                                   |\n"
                 "__________________________________________________________________________" << std::endl;
}
