set(VIEWS views)

add_library(
        ${VIEWS}
        include/View.hpp src/View.cpp)
target_link_libraries(${VIEWS} PUBLIC models utils)
target_include_directories(${VIEWS} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
