//
// Created by mimor on 13-Nov-22.
//

#ifndef TASK_1_VIEW_HPP
#define TASK_1_VIEW_HPP


#include <string>
#include <ostream>
#include <DataStorage.hpp>
#include <CellField.hpp>

class View {
private:
    char aliveCellSymbol = '*';
    char deadCellSymbol = '.';
    std::string title;
    void writeStream(std::ostream& os, const CellField& field) const;
    void generateTitle(DataStorage& dataStorage);
public:
    explicit View(DataStorage& dataStorage);
    void display(const CellField& field) const;
    void save(const std::string& filename, const CellField& field) const;
    void printHelp() const;
};


#endif //TASK_1_VIEW_HPP
