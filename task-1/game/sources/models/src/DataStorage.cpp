//
// Created by mimor on 16-Oct-22.
//

#include "DataStorage.hpp"

std::string DataStorage::getUniverseName() const {
    return this->universeName;
}

const std::set<std::pair<int, int>>& DataStorage::getStartAliveCells() const {
    return this->startAliveCells;
}

std::map<int, bool>& DataStorage::getCellsForBirth() {
    return this->cellsForBirth;
}

std::map<int, bool>& DataStorage::getCellsForSurvival() {
    return this->cellsForSurvival;
}

void DataStorage::setUniverseName(std::string name) {
    this->universeName = name;
}

void DataStorage::addStartAliveCell(std::pair<int, int> toAddCell) {
    this->startAliveCells.insert(toAddCell);
}

void DataStorage::setCellsForBirth(const std::vector<int>& numbers) {
    for (auto number : numbers)
        this->cellsForBirth[number] = true;
}

void DataStorage::setCellsForSurvival(const std::vector<int>& numbers) {
    for (auto number : numbers)
        this->cellsForSurvival[number] = true;
}

int DataStorage::getWidth() const {
    return this->width;
}

int DataStorage::getHeight() const {
    return this->height;
}

void DataStorage::setWidth(int w) {
    this->width = w;
}

void DataStorage::setHeight(int h) {
    this->height = h;
}
