//
// Created by mimor on 17-Oct-22.
//

#include "CommandLineArgs.hpp"
#include "CustomException.hpp"


int CommandLineArgs::getIterations() const {
    return this->iterations;
}

const std::string& CommandLineArgs::getInputFile() const {
    return this->inputFile;
}

const std::string& CommandLineArgs::getOutputFile() const {
    return this->outputFile;
}

void CommandLineArgs::setIterations(int t) {
    this->iterations = t;
}

void CommandLineArgs::setInputFile(std::string file) {
    this->inputFile = file;
}

void CommandLineArgs::setOutputFile(std::string file) {
    this->outputFile = file;
}
