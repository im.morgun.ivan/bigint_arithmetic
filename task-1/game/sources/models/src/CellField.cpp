//
// Created by mimor on 16-Oct-22.
//

#include <iostream>
#include "CellField.hpp"

const Cell& CellField::getCell(int x, int y) const {
    return this->field.at(x).at(y);
}

void CellField::setCellAlive(int x, int y, bool alive) {
    this->field.at(x).at(y).setAlive(alive);
}

int CellField::getHeight() const {
    return this->field.at(0).size();
}

int CellField::getWidth() const {
    return this->field.size();
}

CellField& CellField::operator=(const CellField& cellField) {
    if (this != &cellField) {
        this->field = cellField.field;
    }
    return *this;
}

CellField::CellField(int width, int height) {
    this->field.resize(height);
    for (int i = 0; i < height; i++) {
        this->field[i].resize(width, Cell());
    }
}
