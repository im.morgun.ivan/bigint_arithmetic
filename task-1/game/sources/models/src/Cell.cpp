//
// Created by mimor on 16-Oct-22.
//

#include "../include/Cell.hpp"

bool Cell::getAlive() const {
    return this->alive;
}

void Cell::setAlive(bool newAlive) {
    this->alive = newAlive;
}
