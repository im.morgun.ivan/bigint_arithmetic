//
// Created by mimor on 16-Oct-22.
//

#ifndef TASK_1_DATASTORAGE_HPP
#define TASK_1_DATASTORAGE_HPP

#include <string>
#include <vector>
#include <map>
#include <set>
#include "CommandLineArgs.hpp"

class DataStorage {
private:
    std::string universeName = "Default";
    std::set<std::pair<int, int>> startAliveCells;
    std::map<int, bool> cellsForBirth;
    std::map<int, bool> cellsForSurvival;
    int width = 100;
    int height = 100;
public:
    std::string getUniverseName() const;
    const std::set<std::pair<int, int>>& getStartAliveCells() const;
    std::map<int, bool>& getCellsForBirth();
    std::map<int, bool>& getCellsForSurvival();
    int getWidth() const;
    int getHeight() const;

    void setUniverseName(std::string name);
    void addStartAliveCell(std::pair<int, int> toAddCell);
    void setCellsForBirth(const std::vector<int>& numbers);
    void setCellsForSurvival(const std::vector<int>& numbers);
    void setWidth(int w);
    void setHeight(int h);
};


#endif //TASK_1_DATASTORAGE_HPP
