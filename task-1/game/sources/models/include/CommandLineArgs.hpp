//
// Created by mimor on 17-Oct-22.
//

#ifndef TASK_1_COMMANDLINEARGS_HPP
#define TASK_1_COMMANDLINEARGS_HPP


#include <string>

class CommandLineArgs {
private:
    int iterations = 0;
    std::string inputFile = "default.in";
    std::string outputFile;
public:
    int getIterations() const;
    const std::string& getInputFile() const;
    const std::string& getOutputFile() const;

    void setIterations(int t);
    void setInputFile(std::string file);
    void setOutputFile(std::string file);
};


#endif //TASK_1_COMMANDLINEARGS_HPP
