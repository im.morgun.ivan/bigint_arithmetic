//
// Created by mimor on 16-Oct-22.
//

#ifndef TASK_1_CELLFIELD_HPP
#define TASK_1_CELLFIELD_HPP

#include <vector>
#include "Cell.hpp"

class CellField {
private:
    std::vector<std::vector<Cell>> field;
public:
    const Cell& getCell(int x, int y) const;
    int getWidth() const;
    int getHeight() const;
    void setCellAlive(int x, int y, bool alive);

    CellField(int x, int y);

    CellField& operator=(const CellField& cellField);
};


#endif //TASK_1_CELLFIELD_HPP
