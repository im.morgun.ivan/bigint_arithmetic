//
// Created by mimor on 16-Oct-22.
//

#ifndef TASK_1_CELL_H
#define TASK_1_CELL_H


class Cell {
private:
    bool alive = false;
public:
    bool getAlive() const;
    void setAlive(bool newAlive);
};


#endif //TASK_1_CELL_H
