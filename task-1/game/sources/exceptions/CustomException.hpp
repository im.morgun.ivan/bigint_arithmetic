//
// Created by mimor on 12-Nov-22.
//

#ifndef TASK_1_CUSTOMEXCEPTION_HPP
#define TASK_1_CUSTOMEXCEPTION_HPP


class CustomException : public std::exception {
private:
    std::string description = "Custom exception";
public:
    explicit CustomException(const std::string& desc) {
        this->description = desc;
    }

    const char* what() const noexcept override {
        return this->description.c_str();
    }
};


#endif //TASK_1_CUSTOMEXCEPTION_HPP
