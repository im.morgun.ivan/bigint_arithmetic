//
// Created by mimor on 17-Oct-22.
//

#include "LogicController.hpp"

void LogicController::calculateField(DataStorage dataStorage, int iterations) {
    for (int step = 0; step < iterations; step++) {
        this->previousField = this->currentField;
        for (int x = 0; x < this->previousField.getWidth(); x++) {
            for (int y = 0; y < this->previousField.getHeight(); y++) {
                int aliveNeighboursNumber = Utils::CountAliveNeighbours(x, y, this->previousField);
                if (this->previousField.getCell(x, y).getAlive()) {
                    if (!Utils::GetMapValueWithDefault(dataStorage.getCellsForSurvival(), aliveNeighboursNumber,
                                                       false))
                        this->currentField.setCellAlive(x, y, false);
                }
                else {
                    if (Utils::GetMapValueWithDefault(dataStorage.getCellsForBirth(), aliveNeighboursNumber,
                                                      false)) {
                        this->currentField.setCellAlive(x, y, true);
                    }
                }
            }
        }
    }
}

const CellField &LogicController::getCurrentField() const {
    return this->currentField;
}

LogicController::LogicController(int width, int height, const std::set<std::pair<int, int>> &aliveCells) {
    this->currentField = CellField(width, height);
    for (auto point : aliveCells) {
        this->currentField.setCellAlive(point.first, point.second, true);
    }
}
