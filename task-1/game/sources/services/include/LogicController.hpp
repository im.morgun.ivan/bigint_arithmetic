//
// Created by mimor on 17-Oct-22.
//

#ifndef TASK_1_LOGICCONTROLLER_HPP
#define TASK_1_LOGICCONTROLLER_HPP

#include "DataStorage.hpp"
#include "CellField.hpp"
#include "Utils.hpp"

class LogicController {
private:
    CellField currentField = CellField(0, 0);
    CellField previousField = CellField(0, 0);
public:
    void calculateField(DataStorage dataStorage, int iterations);
    const CellField& getCurrentField() const;

    LogicController(int width, int height, const std::set<std::pair<int, int>> &aliveCells);
};


#endif //TASK_1_LOGICCONTROLLER_HPP
