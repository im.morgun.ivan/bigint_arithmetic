//
// Created by mimor on 10-Jan-23.
//
#include <stdexcept>


class CSVException : public std::runtime_error {
protected:
    CSVException(int lineNumber, int columnNumber, const std::string& description, const std::string& type = "CSV");
};

class FileException : public CSVException {
public:
    explicit FileException(int lineNumber, int columnNumber);
};

class ParserException : public CSVException {
public:
    explicit ParserException(int lineNumber, int columnNumber);
};



