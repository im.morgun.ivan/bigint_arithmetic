//
// Created by mimor on 10-Jan-23.
//
#include "../include/exceptions.hpp"


CSVException::CSVException(int lineNumber, int columnNumber,
                           const std::string& description,
                           const std::string& type)
                           : std::runtime_error("[" + type + " exception] in line " + std::to_string(lineNumber) +
                           ", column " + std::to_string(columnNumber) + ": " + description) {};

FileException::FileException(int lineNumber, int columnNumber)
    : CSVException(lineNumber, columnNumber, "some shit happened with you CSV file") {};

ParserException::ParserException(int lineNumber, int columnNumber)
    : CSVException(lineNumber, columnNumber, "validation error") {};
