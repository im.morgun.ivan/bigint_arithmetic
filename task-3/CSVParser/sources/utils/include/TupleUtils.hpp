//
// Created by mimor on 02-Jan-23.
//

#ifndef CSVPARSER_TUPLEUTILS_HPP
#define CSVPARSER_TUPLEUTILS_HPP

#include <string>
#include <ostream>
#include <tuple>


template<char delimiter = ',', int index = 0, typename ...Args>
std::ostream& PrintTuple(std::ostream& out, const std::tuple<Args...>& tuple) {
    if constexpr (index == sizeof...(Args)) {
        return out;
    } else {
        out << std::get<index>(tuple);
        if (index < sizeof...(Args) - 1)
            out << delimiter;
        return PrintTuple<delimiter, index + 1>(out, tuple);
    }
}

template<typename ...Args>
auto& operator<<(std::ostream& out, const std::tuple<Args...>& tuple) {
    return PrintTuple(out, tuple);
}

#endif //CSVPARSER_TUPLEUTILS_HPP
