set(UTILS utils)
add_library(
        ${UTILS} INTERFACE
        include/TupleUtils.hpp)
target_include_directories(${UTILS} INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include")
