//
// Created by mimor on 10-Jan-23.
//

#ifndef CSVPARSER_CSVPARSER_HPP
#define CSVPARSER_CSVPARSER_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include "exceptions.hpp"

template <typename ...Args>
class CSVParser {
private:
    std::ifstream file;
    std::tuple<Args...> tuple;

    char lineDelimiter;
    char columnDelimiter;
    char shield;

    int currentLineNumber = 0;
    int currentColumnNumber = 0;

    void skipLines(int linesNumber) {
        for (int i = 0; i < linesNumber; i++) {
            getLine(file, lineDelimiter);
            currentLineNumber++;
        }
    }

    std::string getLine(std::istream& stream, char delimiter) {
        std::string line;
        char currentSymbol;
        bool shielded = false;
        while (stream.get(currentSymbol) && (shielded || currentSymbol != delimiter)) {
            if (currentSymbol == shield) {
                shielded = !shielded;
            }
            line.push_back(currentSymbol);
        }
        return line;
    }

    std::string removeShields(std::string& line) {
        std::string newLine;
        for (char c : line) {
            if (c != shield) {
                newLine += c;
            }
        }
        return newLine;
    }

    template<typename Type>
    Type parseValue(std::string& stringValue) {
        if constexpr (std::is_same_v<Type, std::string>) {
            return stringValue;
        }
        std::stringstream stream(stringValue);
        Type value;
        stream >> value;
        if (!stream) {
            throw ParserException(currentLineNumber, currentColumnNumber);
        }
        return value;
    }

    template<int columnIndex = 0>
    void parseTuple(std::stringstream& lineStream) {
        if constexpr (columnIndex == sizeof...(Args)) {
            return;
        } else {
            std::string line = getLine(lineStream, columnDelimiter);
            currentColumnNumber++;
            line = removeShields(line);
            std::get<columnIndex>(tuple) = parseValue<std::tuple_element_t<columnIndex, std::tuple<Args...>>>(line);
            parseTuple<columnIndex + 1>(lineStream);
        }

    }

    void next() {
        std::string line = getLine(file, lineDelimiter);
        currentLineNumber++;
        currentColumnNumber = 0;
        std::stringstream lineStream(line);
        parseTuple(lineStream);
    }

public:
    CSVParser(std::string filename, int skipLinesNumber = 0, char lineDelimiter = '\n',
              char columnDelimiter = ',', char shield = '"')
            : lineDelimiter(lineDelimiter), columnDelimiter(columnDelimiter), shield(shield) {
        file.open(filename);
        if (!file.is_open()) {
            throw FileException(0, 0);
        }
        skipLines(skipLinesNumber);
        next();
    }

    class InputIterator : public std::iterator<std::input_iterator_tag, std::tuple<Args...>> {
        friend class CSVParser;

    private:
        CSVParser<Args...>* iteratorPointer;
    public:
        InputIterator(InputIterator &iterator) : iteratorPointer(iterator.iteratorPointer) {}
        explicit InputIterator(CSVParser<Args...>* pointer = nullptr) : iteratorPointer(pointer) {}

        InputIterator operator++() {
            if (iteratorPointer->file.eof()) {
                iteratorPointer = nullptr;
                return *this;
            }
            iteratorPointer->next();
            return *this;
        }

        InputIterator operator=(const InputIterator inputIterator) {
            iteratorPointer = inputIterator.iteratorPointer;
            return this;
        }

        typename InputIterator::reference operator*() const { return iteratorPointer->tuple; };
        typename InputIterator::pointer operator->() { return &(iteratorPointer->tuple); };

        friend bool operator==(const InputIterator first, const InputIterator second) {
            return first.iteratorPointer == second.iteratorPointer;
        }

        friend bool operator!=(const InputIterator first, const InputIterator second) {
            return first.iteratorPointer != second.iteratorPointer;
        }
    };

    InputIterator begin() { return InputIterator(this); }
    InputIterator end() { return InputIterator(); }
};


#endif //CSVPARSER_CSVPARSER_HPP
