//
// Created by mimor on 02-Jan-23.
//

#include "TupleUtils.hpp"
#include "CSVParser.hpp"
#include <tuple>
#include <iostream>

int main() {
    try {
        CSVParser<int, int, float, std::string> parser("./assets/test.csv");
        for (std::tuple<int, int, float, std::string>& t : parser) {
            std::cout << t << std::endl;
        }
    } catch (const std::exception& exception) {
        std::cout << exception.what();
    }
}

