//
// Created by mimor on 29-Nov-22.
//

#ifndef TASK_2_CONVERTERINTERFACE_HPP
#define TASK_2_CONVERTERINTERFACE_HPP

#include <vector>
#include "WAVStructures.hpp"
#include "Utils.hpp"
#include "converter_exceptions.hpp"


using ConverterParams = std::vector<std::string>;
using AllSamples = std::vector<SampleBuffer>;
using ParamFormats = std::vector<bool>;

class ConverterInterface {
private:
    virtual ParamFormats paramsFormat() = 0;
protected:
    virtual std::string converterName() = 0;

    void validate(const ConverterParams& params) {
        if (params.size() != paramsFormat().size())
            throw InvalidParamsNumber(
                    converterName(),
                    (int) params.size(),
                    (int) paramsFormat().size());
        for (int i = 0; i < params.size(); i++) {
            if (paramsFormat()[i] && !Utils::IsLink(params[i]))
                throw InvalidLinkParam(converterName(), params[i]);
            else if (!paramsFormat()[i] && !Utils::IsInt(params[i]))
                throw InvalidIntParam(converterName(), params[i]);
        }
    }
public:
    virtual void process(SampleBuffer& sampleBuffer, const AllSamples& allSamples) = 0;
};



#endif //TASK_2_CONVERTERINTERFACE_HPP
