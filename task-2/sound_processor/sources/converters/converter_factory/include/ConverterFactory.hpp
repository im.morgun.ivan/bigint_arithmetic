//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_CONVERTERFACTORY_HPP
#define TASK_2_CONVERTERFACTORY_HPP

#include "ConverterInterface.hpp"
#include "MixConverter.hpp"
#include "MuteConverter.hpp"
#include "InverseConverter.hpp"
#include "converter_exceptions.hpp"


class ConverterFactory {
public:
    static ConverterInterface* CreateConverter(std::string converterName, const ConverterParams& params);
};


#endif //TASK_2_CONVERTERFACTORY_HPP
