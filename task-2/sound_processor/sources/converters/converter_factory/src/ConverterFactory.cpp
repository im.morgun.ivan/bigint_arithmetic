//
// Created by mimor on 30-Nov-22.
//

#include "../include/ConverterFactory.hpp"


ConverterInterface* ConverterFactory::CreateConverter(std::string converterName, const ConverterParams &params) {
    if (converterName == "mix")
        return new MixConverter(params);
    if (converterName == "mute")
        return new MuteConverter(params);
    if (converterName == "inverse")
        return new InverseConverter(params);
    throw InvalidConverterName(converterName);
}
