#include "../include/converter_exceptions.hpp"


ConverterException::ConverterException(const std::string &converterName, const std::string &description)
    : std::invalid_argument("[Converter exception] " + converterName + ": " + description) {};


InvalidParamsNumber::InvalidParamsNumber(const std::string &converterName, int gotNumber, int requiredNumber)
    : ConverterException(converterName,
                    "Invalid params number, got " + std::to_string(gotNumber) +
                    " instead of " + std::to_string(requiredNumber) + " params") {}


InvalidIntParam::InvalidIntParam(const std::string &converterName, const std::string &param)
    : ConverterException(converterName,
                    "Invalid int param, got value: " + param) {}


InvalidLinkParam::InvalidLinkParam(const std::string &converterName, const std::string &param)
    : ConverterException(converterName,
                    "Invalid link param, got value " + param) {}


InvalidConverterName::InvalidConverterName(const std::string &converterName)
    : ConverterException("Converter factory", "Invalid converter name: " + converterName) {}
