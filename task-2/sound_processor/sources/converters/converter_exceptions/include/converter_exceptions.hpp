#include <stdexcept>


class ConverterException : public std::invalid_argument {
protected:
    ConverterException(const std::string& converterName, const std::string& description);
};

class InvalidParamsNumber : public ConverterException {
public:
    explicit InvalidParamsNumber(const std::string& converterName, int gotNumber, int requiredNumber);
};

class InvalidIntParam : public ConverterException {
public:
    explicit InvalidIntParam(const std::string& converterName, const std::string& param);
};

class InvalidLinkParam : public ConverterException {
public:
    explicit InvalidLinkParam(const std::string& converterName, const std::string& param);
};

class InvalidConverterName : public ConverterException {
public:
    explicit InvalidConverterName(const std::string& converterName);
};
