//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_MIXCONVERTER_HPP
#define TASK_2_MIXCONVERTER_HPP

#include "ConverterInterface.hpp"


class MixConverter : public ConverterInterface{
private:
    std::string converterName() override { return "mix"; };

    int mixingSampleNumber;
    int beginSecond;
    int currentSecond = 0;

    ParamFormats paramsFormat() override;
public:
    void process(SampleBuffer& sampleBuffer, const AllSamples& allSamples) override;

    explicit MixConverter(const ConverterParams& params);
};


#endif //TASK_2_MIXCONVERTER_HPP
