//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_MUTECONVERTER_HPP
#define TASK_2_MUTECONVERTER_HPP

#include "ConverterInterface.hpp"

class MuteConverter : public ConverterInterface{
private:
    std::string converterName() override { return "mute"; };

    int currentSecond = 0;
    int beginSecond;
    int endSecond;

    ParamFormats paramsFormat() override;
public:
    void process(SampleBuffer& sampleBuffer, const AllSamples& allSamples) override;

    explicit MuteConverter(const ConverterParams& params);
};


#endif //TASK_2_MUTECONVERTER_HPP
