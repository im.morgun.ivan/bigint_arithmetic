//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_INVERSECONVERTER_HPP
#define TASK_2_INVERSECONVERTER_HPP

#include "ConverterInterface.hpp"


class InverseConverter : public ConverterInterface {
private:
    std::string converterName() override { return "inverse"; };

    int beginSecond;
    int endSecond;
    int currentSecond = 0;

    ParamFormats paramsFormat() override;
public:
    void process(SampleBuffer& sampleBuffer, const AllSamples& allSamples) override;

    explicit InverseConverter(const ConverterParams& params);
};


#endif //TASK_2_INVERSECONVERTER_HPP
