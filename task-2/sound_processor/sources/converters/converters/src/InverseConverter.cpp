//
// Created by mimor on 30-Nov-22.
//

#include <iostream>
#include "InverseConverter.hpp"

InverseConverter::InverseConverter(const ConverterParams &params) {
    this->validate(params);
    this->beginSecond = std::stoi(params[0]);
    this->endSecond = std::stoi(params[1]);
}

void InverseConverter::process(SampleBuffer &sampleBuffer, const AllSamples &allSamples) {
    if (this->currentSecond >= this->beginSecond && this->currentSecond <= this->endSecond) {
        for (short & s : sampleBuffer) {
            short ts = s;
            short reversed = 0;
            int pos = sizeof(short) * 8 - 1;
            while (pos >= 0 && ts) {
                if (ts & 1) {
                    reversed = reversed | (1 << pos);
                }
                ts >>= 1;
                pos--;
            }
            s = reversed;
        }
    }
    this->currentSecond++;
}

ParamFormats InverseConverter::paramsFormat() {
    return (ParamFormats) {false, false};
}
