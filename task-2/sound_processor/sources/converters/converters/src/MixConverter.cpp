//
// Created by mimor on 30-Nov-22.
//

#include <iostream>
#include "MixConverter.hpp"

MixConverter::MixConverter(const ConverterParams &params) {
    this->validate(params);
    this->mixingSampleNumber = std::stoi(params[0].substr(1));
    std::cout << this->mixingSampleNumber << std::endl;
    this->beginSecond = std::stoi(params[1]);
}

void MixConverter::process(SampleBuffer &sampleBuffer, const AllSamples &allSamples) {
    if (this->currentSecond >= this->beginSecond) {
        for (int i = 0; i < sampleBuffer.size(); i++) {
            sampleBuffer[i] = (sampleBuffer[i] / 2) + (allSamples[this->mixingSampleNumber][i] / 2);
        }
    }
    this->currentSecond++;
}

ParamFormats MixConverter::paramsFormat() {
    return (ParamFormats) {true, false};
}
