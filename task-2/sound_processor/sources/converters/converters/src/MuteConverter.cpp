//
// Created by mimor on 30-Nov-22.
//

#include "MuteConverter.hpp"


void MuteConverter::process(SampleBuffer &sampleBuffer, const AllSamples &allSamples) {
    if (this->currentSecond >= this->beginSecond && this->currentSecond <= this->endSecond)
        sampleBuffer.fill(0);
    this->currentSecond++;
}

ParamFormats MuteConverter::paramsFormat() {
    return (ParamFormats) {false, false};
}

MuteConverter::MuteConverter(const ConverterParams &params) {
    this->validate(params);

    this->beginSecond = std::stoi(params[0]);
    this->endSecond = std::stoi(params[1]);
}
