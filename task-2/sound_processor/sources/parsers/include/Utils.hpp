//
// Created by mimor on 29-Nov-22.
//

#ifndef TASK_2_UTILS_HPP
#define TASK_2_UTILS_HPP


#include <string>
#include <regex>

class Utils {
public:
    static bool IsLink(const std::string& param);
    static bool IsInt(const std::string param);
};


#endif //TASK_2_UTILS_HPP
