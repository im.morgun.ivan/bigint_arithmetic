//
// Created by mimor on 29-Nov-22.
//

#ifndef TASK_2_WAVSTRUCTURES_HPP
#define TASK_2_WAVSTRUCTURES_HPP

#include <cstdint>
#include <array>

const uint32_t RIFF = 0x46464952;
const uint32_t WAVE = 0x45564157;
const uint32_t FMT_ = 0x20746d66;
const uint32_t DATA = 0x61746164;

const uint16_t AUDIO_FORMAT_PCM = 1;
const uint16_t NUM_CHANNELS = 1;
const uint32_t SAMPLING_RATE = 44100;
const uint16_t BITS_PER_BYTE = 8;
const uint16_t BITS_PER_SAMPLE = BITS_PER_BYTE * sizeof(int16_t);
const uint16_t BLOCK_ALIGN = BITS_PER_SAMPLE * NUM_CHANNELS / BITS_PER_BYTE;
const uint32_t BYTE_RATE = BLOCK_ALIGN * SAMPLING_RATE;

typedef struct header_chunk_t
{
    uint32_t id;
    uint32_t size;
} header_chunk_t;

typedef struct fmt_chunk_t
{
    uint16_t audio_format = AUDIO_FORMAT_PCM;
    uint16_t num_channels = NUM_CHANNELS;
    uint32_t sampling_rate = SAMPLING_RATE;
    uint32_t byte_rate = BYTE_RATE;
    uint16_t block_align = BLOCK_ALIGN;
    uint16_t bits_per_sample = BITS_PER_SAMPLE;
} fmt_chunk_t;

using Sample = int16_t;
using SampleBuffer = std::array<Sample, SAMPLING_RATE>;


#endif //TASK_2_WAVSTRUCTURES_HPP
