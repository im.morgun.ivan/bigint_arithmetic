//
// Created by mimor on 29-Nov-22.
//

#ifndef TASK_2_WAVREADER_HPP
#define TASK_2_WAVREADER_HPP


#include <fstream>
#include "WAVStructures.hpp"


class WAVReader {
private:
    inline static const std::string filepathPrefix = "./assets/";
    std::ifstream wavFile;
    void open(const std::string& filename);
public:
    bool fillBuffer(SampleBuffer& sampleBuffer);
    void findChunk(uint32_t chunkID);
    void readHeader();

    explicit WAVReader(const std::string& filename);
};


#endif //TASK_2_WAVREADER_HPP
