//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_CONFIGPARSER_HPP
#define TASK_2_CONFIGPARSER_HPP

#include <fstream>
#include <vector>


using ConverterParams = std::vector<std::string>;

typedef struct command_t {
    std::string converterName;
    ConverterParams params;
} command_t;

using Commands = std::vector<command_t>;


class ConfigParser {
private:
    inline static const std::string filepathPrefix = "./assets/";
    std::ifstream cfgFile;
public:
    Commands getCommands();
    explicit ConfigParser(const std::string& filename);
};


#endif //TASK_2_CONFIGPARSER_HPP
