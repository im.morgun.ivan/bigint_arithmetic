//
// Created by mimor on 30-Nov-22.
//

#ifndef TASK_2_WAVWRITER_HPP
#define TASK_2_WAVWRITER_HPP


#include <string>
#include <fstream>
#include "WAVStructures.hpp"

class WAVWriter {
private:
    inline static const std::string filepathPrefix = "./assets/";
    std::ofstream wavFile;

    void open(std::string filename);
    void writeHeader();

public:
    void updateHeader();
    void writeBuffer(SampleBuffer& sampleBuffer);
    explicit WAVWriter(const std::string& filename);
    ~WAVWriter();
};


#endif //TASK_2_WAVWRITER_HPP
