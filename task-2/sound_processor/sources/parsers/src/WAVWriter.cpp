//
// Created by mimor on 30-Nov-22.
//

#include <regex>
#include <iostream>
#include "WAVWriter.hpp"

WAVWriter::WAVWriter(const std::string& filename) {
    this->open(filename);
    this->writeHeader();
}

void WAVWriter::open(std::string filename) {
    if (!std::regex_match(filename, std::regex(R"(.+\.wav)", std::regex_constants::ECMAScript)))
        throw std::runtime_error("Invalid file: " + filename);

    this->wavFile.open(WAVWriter::filepathPrefix + filename, std::ios::out);
    if (this->wavFile.bad())
        throw std::runtime_error("Can't open file: " + filename);
}

WAVWriter::~WAVWriter() {
    this->updateHeader();
}

void WAVWriter::writeHeader() {
    header_chunk_t wrapper = {RIFF, 0};
    uint32_t format = WAVE;
    header_chunk_t fmtHeader = {FMT_, sizeof(fmt_chunk_t)};
    fmt_chunk_t fmtData;
    header_chunk_t dataHeader = {DATA, 0};

    this->wavFile.write(reinterpret_cast<char *> (&wrapper), sizeof(wrapper));
    this->wavFile.write(reinterpret_cast<char *> (&format), sizeof(format));
    this->wavFile.write(reinterpret_cast<char *> (&fmtHeader), sizeof(fmtHeader));
    this->wavFile.write(reinterpret_cast<char *> (&fmtData), sizeof(fmtData));
    this->wavFile.write(reinterpret_cast<char *> (&dataHeader), sizeof(dataHeader));
}

void WAVWriter::updateHeader() {
    this->wavFile.seekp(0, std::ios::end);
    uint32_t filesize = this->wavFile.tellp();

    filesize -= sizeof(header_chunk_t);
    this->wavFile.seekp(sizeof(RIFF), std::ios::beg);
    this->wavFile.write(reinterpret_cast<char *>(&filesize), sizeof(filesize));

    int predataPartSize = sizeof(uint32_t) + sizeof(header_chunk_t) + sizeof(fmt_chunk_t) + sizeof(header_chunk_t);
    filesize -= predataPartSize;
    this->wavFile.seekp(predataPartSize - sizeof(uint32_t), std::ios::cur);

    this->wavFile.write(reinterpret_cast<char *>(&filesize), sizeof(filesize));
}

void WAVWriter::writeBuffer(SampleBuffer &sampleBuffer) {
    this->wavFile.write(reinterpret_cast<char *>(&sampleBuffer), sizeof(Sample) * sampleBuffer.size());
    if (this->wavFile.bad())
        throw std::runtime_error("Can't write to the file");
}
