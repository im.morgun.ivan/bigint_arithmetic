//
// Created by mimor on 29-Nov-22.
//

#include "../include/WAVReader.hpp"
#include <regex>
#include <iostream>


WAVReader::WAVReader(const std::string& filename) {
    this->open(filename);
    this->readHeader();
}

void WAVReader::open(const std::string& filename) {
    if (!std::regex_match(filename, std::regex(R"(.+\.wav)", std::regex_constants::ECMAScript)))
        throw std::runtime_error("Invalid file: " + filename);

    this->wavFile.open(WAVReader::filepathPrefix + filename, std::ios::binary);
    if (this->wavFile.bad())
        throw std::runtime_error("Can't open file: " + filename);
}

void WAVReader::readHeader() {
    header_chunk_t headerChunk;
    this->wavFile.read(reinterpret_cast<char *>(&headerChunk), sizeof(headerChunk));

    if (this->wavFile.bad() || headerChunk.id != RIFF)
        throw std::runtime_error("Incorrect header chunk ID");

    uint32_t formatType;
    this->wavFile.read(reinterpret_cast<char *>(&formatType), sizeof(formatType));
    if (this->wavFile.bad() || formatType != WAVE)
        throw std::runtime_error("Unsupported format type");

    this->findChunk(FMT_);
    fmt_chunk_t formatChunk;
    this->wavFile.read(reinterpret_cast<char *>(&formatChunk), sizeof(formatChunk));
    if (this->wavFile.bad())
        throw std::runtime_error("Incorrect format chunk in file ");

    if (formatChunk.audio_format != AUDIO_FORMAT_PCM)
        throw std::runtime_error("Unsupported audio format");
    if (formatChunk.num_channels != NUM_CHANNELS)
        throw std::runtime_error("Unsupported number of channels");
    if (formatChunk.bits_per_sample != BITS_PER_SAMPLE)
        throw std::runtime_error("Unsupported bits per sample number");
    if (formatChunk.sampling_rate != SAMPLING_RATE)
        throw std::runtime_error("Unsupported sampling rate");

    this->findChunk(DATA);
}

void WAVReader::findChunk(uint32_t chunkID) {
    header_chunk_t headerChunk;
    this->wavFile.read(reinterpret_cast<char *>(&headerChunk), sizeof(headerChunk));
    while (this->wavFile.good() && headerChunk.id != chunkID) {
        this->wavFile.seekg(headerChunk.size, std::ios::cur);
        this->wavFile.read(reinterpret_cast<char *>(&headerChunk), sizeof(headerChunk));
    }
    if (this->wavFile.bad())
        throw std::runtime_error("Chunk with ID " + std::to_string(chunkID) + " wasn't found");
}

bool WAVReader::fillBuffer(SampleBuffer& sampleBuffer) {
    this->wavFile.read(reinterpret_cast<char *>(&sampleBuffer), sizeof(Sample) * sampleBuffer.size());
    int read = this->wavFile.gcount() / sizeof(Sample);

    std::fill_n(sampleBuffer.begin() + read, sampleBuffer.size() - read, 0);
    return this->wavFile.gcount() != 0;
}
