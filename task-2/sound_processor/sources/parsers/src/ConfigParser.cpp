//
// Created by mimor on 30-Nov-22.
//

#include <regex>
#include <iostream>
#include <ios>
#include "ConfigParser.hpp"

ConfigParser::ConfigParser(const std::string &filename) {
    this->cfgFile.open(ConfigParser::filepathPrefix + filename, std::ios::in);
    if (this->cfgFile.bad())
        throw std::ios_base::failure(filename);
}

Commands ConfigParser::getCommands() {
    Commands commands;
    std::string line;
    while (!this->cfgFile.eof() && this->cfgFile.good()) {
        getline(this->cfgFile, line);
        if (line.empty() || std::regex_match(line, std::regex(R"(#.+)", std::regex_constants::ECMAScript)))
            continue;
        else
            line = line.substr(0,line.find('#'));

        std::istringstream splited(line);
        command_t command;
        splited >> command.converterName;
        std::string t;
        while (splited >> t) {
            command.params.push_back(t);
        }

        commands.push_back(command);
    }
    return commands;
}

