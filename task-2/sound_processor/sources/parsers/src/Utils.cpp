//
// Created by mimor on 29-Nov-22.
//

#include <algorithm>
#include <iostream>
#include "../include/Utils.hpp"

bool Utils::IsLink(const std::string& param) {
    return std::regex_match(param, std::regex(R"(\$\d+\s{0,})", std::regex_constants::ECMAScript));
}

bool Utils::IsInt(const std::string param) {
    return std::regex_match(param, std::regex(R"(\d+\s{0,})", std::regex_constants::ECMAScript));
}
