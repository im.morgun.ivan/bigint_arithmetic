#include <iostream>
#include <vector>
#include <ConfigParser.hpp>
#include "WAVReader.hpp"
#include "WAVWriter.hpp"
#include "ConverterInterface.hpp"
#include "ConverterFactory.hpp"

int main(int argc, char* argv[]) {
    if (argc < 1 || (argv[1] == "-c" && argc < 4)) {
        throw std::runtime_error("Invalid cl args");
        return 1;
    }
    if (argv[1] == "-h") {
        std::cout << "Help";
        return 1;
    }


    try {
        WAVWriter writer(argv[3]);
        ConfigParser configParser(argv[2]);
        WAVReader reader(argv[4]);

        std::vector<WAVReader> readers;
        std::vector<SampleBuffer> buffers(argc - 5);
        for (int i = 5; i < argc; i++) {
            readers.push_back(WAVReader(argv[i]));
        }
        std::vector<ConverterInterface *> convertors;
        Commands commands = configParser.getCommands();
        for (auto com : commands) {
            convertors.push_back(ConverterFactory::CreateConverter(com.converterName, com.params));
        }

        SampleBuffer sampleBuffer;
        while (reader.fillBuffer(sampleBuffer)) {
            for (int i = 0; i < readers.size(); i++) {
                readers[i].fillBuffer(buffers[i]);
            }
            for (auto c: convertors) {
                c->process(sampleBuffer, buffers);
            }
            writer.writeBuffer(sampleBuffer);
        }
        writer.updateHeader();
    }
    catch (const std::exception e) {
        std::cerr << e.what() << std::endl;
    }
}
