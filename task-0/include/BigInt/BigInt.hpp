#include <vector>
#include <string>
#include <iomanip>
#include <algorithm>



class BigInt {
    /**
    * BigInt storage
    *
    * Stores digit values in big-endian order
    */
    std::vector<int> digits;
    /**
    * Sign of BigInt
    *
    * true if value is negative
    * false otherwise
    */
    bool isNegative = false;

    public:
        /**
        * Base of each digit in storage (@see std::vector<int> digits)
        */ 
        static const int MOD = 1000000000;
        /**
        * Max length of each digit in storage (@see std::vector<int> digits)
        */
        static const int DIGIT_SIZE = 9;

        // Getters & Setters
        /**
         * {@code this->isNegative} getter  
         */
        bool getIsNegative() const;
        /**
        * Push @param digitValue to {@code this->digits}
        *
        * @throw {@code std::invalid_argument} if @param digitValue is not in range [0, BigInt::MOD]
        */
        void addDigit(int digitValue);
        /**
        * Set @param value to {@code this->digits} at @param digitIndex position
        *
        * @throw {@code std::out_of_range} if @param digitIndex is not in range [0, this->digits.size())
        * @throw {@code std::invalid_argument} if @param value is not in range [0, BigInt::MOD]
        */
        void setDigit(int digitIndex, int value);
        /**
        * Set @param negative to {@code this->isNegative}
        */
        void setNegative(bool negative);
        /**
        * Add @param value to {@code this->digits} at @param digitIndex position
        */
        void addValueToDigit(int digitIndex, int value);
        /**
        * Resize {@code this->digits} to @param n length with filling zeros
        */
        void resize(int n);
        /**
        * Remove leading zeros from {@code this->digits}
        */
        void removeLeadingZeros();
        /**
        * Shift {@code this->digits} to one digit
        * Equals to multiplaing with {@code BigInt::MOD}
        */
        void shiftRight();
    
        // Constructors
        /**
        * Create default BigInt
        */
        BigInt();
        /**
        * Create BigInt from int
        */
        BigInt(int);
        /**
        * Create BigInt from std::string
        * 
        * @throw std::invalid_argument if input is incorrect
        */
        BigInt(std::string);
        /**
        * Copy constructor
        * Copy each field of input reference into {@code *this}
        */
        BigInt(const BigInt&);
        /**
        * Destructor
        * Call some fields destructors
        */
        ~BigInt();

        /**
        * BigInt to int converter
        * 
        * @return int number which is stored in this BigInt if value less than INT_MAX
        * @return int INT_MAX or INT_MIN otherwise
        */
        explicit operator int() const;

        // Operators
        /**
        * Copy assignment
        * Copy each field of input reference into {@code *this}
        * Works with self-assignment
        */
        BigInt& operator=(const BigInt&);
        /**
        * Unary plus
        */ 
        BigInt operator+() const; 
        /**
        * Unary minus
        */
        BigInt operator-() const; 

        /**
        * Prefix increment
        */
        BigInt& operator++();
        /**
        * Postfix increment
        */
        BigInt operator++(int);
        /**
        * Prefix decrement
        */
        BigInt& operator--();
        /**
        * Postfix decrement
        */
        BigInt operator--(int);

        /**
        * Arithmetical addition
        * Works on @see BigInt operator+(const BigInt&, const BigInt&)
        */
        BigInt& operator+=(const BigInt&);
        /**
        * Arithmetical multiplication
        * Works on @see BigInt operator*(const BigInt&, const BigInt&)
        */
        BigInt& operator*=(const BigInt&);
        /**
        * Arithmetical subtraction
        * Works on @see BigInt operator-(const BigInt&, const BigInt&)
        */
        BigInt& operator-=(const BigInt&);
        /**
        * Arithmetical division
        * Works on @see BigInt operator/(const BigInt&, const BigInt&)
        */
        BigInt& operator/=(const BigInt&);
        /**
        * XOR
        * Works on @see BigInt operator^(const BigInt&, const BigInt&)
        */
        BigInt& operator^=(const BigInt&);
        /**
        * Division with reminder
        * Works on @see BigInt operator%(const BigInt&, const BigInt&)
        */
        BigInt& operator%=(const BigInt&);
        /**
        * Bitwise AND
        * Works on @see BigInt operator&(const BigInt&, const BigInt&)
        */
        BigInt& operator&=(const BigInt&);
        /**
        * Bitwise OR
        * Works on @see BigInt operator|(const BigInt&, const BigInt&)
        */
        BigInt& operator|=(const BigInt&);
        /**
        * Bitwise unary inversion
        */
        BigInt operator~() const;

        /**
        * Logic equality
        */
        bool operator==(const BigInt&) const;
        /**
        * Logic not-equal-to operator
        */
        bool operator!=(const BigInt&) const;
        /**
        * Logic less-than
        */
        bool operator<(const BigInt&) const;
        /**
        * Logic greater-than
        */
        bool operator>(const BigInt&) const;
        /**
        * Logic less-or-equal-to
        */    
        bool operator<=(const BigInt&) const;
        /**
        * Logic greater-or-equal-to
        */
        bool operator>=(const BigInt&) const;

        
        /**
        * BigInt to string converter
        *
        * @return string interpretation of number which is stored in this BigInt
        */
        operator std::string() const;

        /**
        * This BigInt entity size counter
        *
        * @return summury size of all entity fields
        */
        size_t size() const; 


        friend BigInt operator+(const BigInt&, const BigInt&);
        friend BigInt operator-(const BigInt&, const BigInt&);
        friend BigInt operator*(const BigInt&, const BigInt&);
        friend BigInt operator/(const BigInt&, const BigInt&);
        friend BigInt operator^(const BigInt&, const BigInt&);
        friend BigInt operator%(const BigInt&, const BigInt&);
        friend BigInt operator&(const BigInt&, const BigInt&);
        friend BigInt operator|(const BigInt&, const BigInt&);
        friend std::ostream& operator<<(std::ostream& os, const BigInt& bigInt);
        friend std::vector<char> BigIntToBin(BigInt);
};

/**
* Addition
* Works on digit-by-digit addition with carring {@code (int) digitSum / BigInt::MOD}
*/
BigInt operator+(const BigInt&, const BigInt&);
/**
* Subtraction
* Works on digit-by-digit subtraction with carring from the head digit 
*/
BigInt operator-(const BigInt&, const BigInt&);
/**
* Multiplication
* Works on digit-by-digit multiplication into long long variable (each multiplication) to write it into new BigInt
*/
BigInt operator*(const BigInt&, const BigInt&);
/**
* Division
* Works on O(log10) * multiplication O algorithm
* Performs selection of right result using addition
*/
BigInt operator/(const BigInt&, const BigInt&);
/**
* XOR
* Works on @see std::vector<char> BigIntToBin(BigInt) and @see BigInt binToBigInt(std::vector<char> binary)
* by using condition for each bit in binary form of BigInt value 
*/
BigInt operator^(const BigInt&, const BigInt&);
/**
* Division with reminder
* Works on @see division
*/
BigInt operator%(const BigInt&, const BigInt&);
/**
* Bitwise AND
* Works on @see std::vector<char> BigIntToBin(BigInt) and @see BigInt binToBigInt(std::vector<char> binary)
* by using condition for each bit in binary form of BigInt value
*/
BigInt operator&(const BigInt&, const BigInt&);
/**
* Bitwise OR
* Works on @see std::vector<char> BigIntToBin(BigInt) and @see BigInt binToBigInt(std::vector<char> binary)
* by using condition for each bit in binary form of BigInt value
*/
BigInt operator|(const BigInt&, const BigInt&);

/**
* BitInt to binary form translator
* @return std::vector<char> where each element is 0 or 1 
*/ 
std::vector<char> BigIntToBin(BigInt);
/**
 * Math pow
 * Works on O(degree * multiplication)
 */
BigInt pow(const BigInt& value, int degree);
/**
 * Binary form of number to normal BigInt translator
 * @return BigInt that is number from @param binary vector of bits
 * Opposite action to @see std::vector<char> BigIntToBin(BigInt)
 */
BigInt binToBigInt(std::vector<char> binary);

/**
 * BitInt to ostream writer
 */
std::ostream& operator<<(std::ostream& os, const BigInt& bigInt);
