#include <BigInt.hpp>
#include <iostream>
#include <climits>

bool BigInt::getIsNegative() const { return this->isNegative; }
void BigInt::addDigit(int digitValue) {
    if (digitValue >= BigInt::MOD || digitValue < 0)
        throw std::invalid_argument("Value must be in range [0, BigInt::MOD]"); 
    this->digits.push_back(digitValue); 
}
void BigInt::setDigit(int digitIndex, int value) {
    if (digitIndex >= this->digits.size() || digitIndex < 0)
        throw std::out_of_range("Digit index must be less than size of digits vector"); 
    if (value >= BigInt::MOD || value < 0)
        throw std::invalid_argument("Value must be in range [0, BigInt::MOD]"); 
    this->digits[digitIndex] = value; 
}
void BigInt::setNegative(bool negative) { this->isNegative = negative; }
void BigInt::addValueToDigit(int digitIndex, int value) { 
    if (digitIndex >= this->digits.size() || digitIndex < 0)
        throw std::out_of_range("Digit index must be less than size of digits vector"); 
    this->digits[digitIndex] += value; 
}
void BigInt::resize(int n) { this->digits.resize(n, 0); }

BigInt pow(const BigInt& value, int degree) {
    BigInt result(1);
    while (degree) {
        result *= value;
        degree--;
    } 
    return result;
}

BigInt binToBigInt(std::vector<char> binary) {
    BigInt result(0);
    for (int i = 0; i < binary.size(); i++) {
        result += BigInt(binary[i]) * pow(BigInt(2), i);
    }
    return result;
}

void BigInt::removeLeadingZeros() {
    if (this->digits.empty())
        return;
    while (this->digits.back() == 0 && this->digits.size() > 1)
        this->digits.pop_back();
}

std::vector<char> BigIntToBin(BigInt number) {
    std::vector<char> binary;
    number.setNegative(false);
    while (number > BigInt(0)) {
        binary.push_back(number.digits[0] % 2);
        number /= 2;
    } 
    std::reverse(binary.begin(), binary.end());
    return binary;
}

void BigInt::shiftRight() {
    this->digits.push_back(this->digits[this->digits.size() - 1]);
    for (int i = this->digits.size() - 2; i > 0; i--)
        this->digits[i] = this->digits[i - 1];
    this->digits[0] = 0;
}

BigInt::BigInt() {
    this->digits.push_back(0);
}

BigInt::BigInt( int value) {
    if (value < 0)
        this->isNegative = true;
    
    value = abs(value);
    do {
        this->digits.push_back(value % this->MOD);
        value /= this->MOD;
    } while (value > 0);
}

BigInt::BigInt(std::string value) {
    try {
        if (value[0] != '-' && (value[0] < '0' || value[0] > '9'))
            throw std::invalid_argument("Invalid argument!");
        for (int i = 1; i < value.size(); i++) {
            if (value[i] < '0' || value[i] > '9')
                throw std::invalid_argument("Invalid argument!");
        }
    }
    catch (std::invalid_argument& ex) {
        std::cout << ex.what();
        exit(0);
    }

    if (value[0] == '-') {
        this->isNegative = true;
        value[0] = '0';
    }
    for (int i = value.size(); i > 0; i -= this->DIGIT_SIZE) {
        int digitValue;
        if (i < this->DIGIT_SIZE)
            digitValue = std::atoi(value.substr(0, value.size() % this->DIGIT_SIZE).c_str());
        else
            digitValue = std::atoi(value.substr(i - this->DIGIT_SIZE, this->DIGIT_SIZE).c_str());
        this->digits.push_back(digitValue);
    }    
    this->removeLeadingZeros();
}

BigInt::BigInt(const BigInt& value) {
    this->isNegative = value.isNegative;
    this->digits = value.digits;
}

BigInt::~BigInt() {
    this->digits.clear();
}

std::ostream& operator<<(std::ostream& os, const BigInt& bigInt) {
    std::vector<int> digits = bigInt.digits;
    if (bigInt.getIsNegative())
        os << '-';

    os << digits.back();
    os.fill('0');
    for (std::vector<int>::reverse_iterator digit = digits.rbegin() + 1; digit != digits.rend(); digit++)
        os << std::setw(9) << *digit;

    return os;
}

bool BigInt::operator==(const BigInt& comparable) const {
    if (this->isNegative != comparable.getIsNegative()) 
        return false;
    if (this->digits.size() != comparable.digits.size())
        return false;
    for (int i = 0; i < this->digits.size(); i++)
        if (this->digits[i] != comparable.digits[i])
            return false;
        
    return true;
}

bool BigInt::operator!=(const BigInt& comparable) const {
    return !(*this == comparable);
}

bool BigInt::operator<(const BigInt& comparable) const {
    if (*this == comparable)
        return false;
    if (this->getIsNegative()) {
        if (comparable.getIsNegative())
            return (-comparable) < (-*this);
        else
            return true;
    }
    else if (comparable.getIsNegative()) {
        return false;
    }
    else {
        if (this->digits.size() != comparable.digits.size()) {
            return this->digits.size() < comparable.digits.size();
        }
        else {
            for (int i = this->digits.size() - 1; i >= 0; i--) {
                if (this->digits[i] != comparable.digits[i])
                    return this->digits[i] < comparable.digits[i];
            }
            return true;
        }
    }
}

bool BigInt::operator>(const BigInt& comparable) const {
    return !(*this == comparable || *this < comparable); 
}

bool BigInt::operator<=(const BigInt& comparable) const {
    return *this == comparable || *this < comparable; 
}

bool BigInt::operator>=(const BigInt& comparable) const {
    return *this == comparable || *this > comparable; 
}

BigInt BigInt::operator+() const {
    return *this;
}

BigInt BigInt::operator-() const {
    BigInt copy(*this);
    copy.isNegative = !copy.isNegative;
    return copy;
} 

BigInt operator+(const BigInt& left, const BigInt& right) {
    if (left.getIsNegative()) {
        if (right.getIsNegative())
            return -(-left + (-right));
        else
            return right - (-left);
    }
    else if (right.getIsNegative())
        return left - (-right);

    BigInt result(left);
    int carry = 0;
    for (int i = 0; i < std::max(result.digits.size(), right.digits.size()) || carry > 0; i++) {
        if (i == result.digits.size())
            result.addDigit(0);
        
        result.addValueToDigit(i, carry + (i < right.digits.size() ? right.digits[i] : 0));
        carry = result.digits[i] >= BigInt::MOD;
        if (carry != 0) 
            result.addValueToDigit(i, -BigInt::MOD);        
    }
    return result;
}

BigInt& BigInt::operator+=(const BigInt& right) {
    return *this = *this + right;
}

BigInt operator-(const BigInt& left, const BigInt& right) {
    if (right.getIsNegative())
        return left + (-right);
    else if (left.getIsNegative())
        return -(-left + right);
    else if (left < right)
        return -(right - left);
    
    BigInt result(left);
    int carry = 0;
    for (int i = 0; i < right.digits.size() || carry != 0; i++) {
        result.addValueToDigit(i, -(carry + (i < right.digits.size() ? right.digits[i] : 0)));
        carry = result.digits[i] < 0;
        if (carry != 0)
            result.addValueToDigit(i, BigInt::MOD);
    }
    result.removeLeadingZeros();
    return result;
}

BigInt& BigInt::operator-=(const BigInt& right) {
    return *this = *this - right;
}

BigInt operator*(const BigInt& left, const BigInt& right) {
    BigInt result(0);
    result.resize(left.digits.size() + right.digits.size() + 1);
    for (int i = 0; i < left.digits.size(); i++) {
        long long carry = 0;
        for (int j = 0; j < right.digits.size() || carry != 0; j++) {
            long long current = result.digits[i + j] + carry + 1LL * left.digits[i] * (j < right.digits.size() ? right.digits[j] : 0);
            long long currentDigitValue = (current % BigInt::MOD);
            int nextDigitCarry = currentDigitValue / BigInt::MOD;
            result.setDigit(i + j, (int) currentDigitValue % BigInt::MOD);
            carry = current / BigInt::MOD + nextDigitCarry;
        }
    }
    result.removeLeadingZeros();
    result.setNegative(left.getIsNegative() != right.getIsNegative());
    return result;
}

BigInt& BigInt::operator*=(const BigInt& right) {
    return *this = *this * right;
}

BigInt operator/(const BigInt& left, const BigInt& right) {
    BigInt dividend(left), divider(right);
    dividend.setNegative(false);
    divider.setNegative(false);
    try {
        if (divider == BigInt(0)) {
            throw std::logic_error("Division by zero!");
        }        
    }
    catch (std::exception& ex) {
        std::cout << ex.what();
        exit(0);
    }
    
    if (dividend < divider)
        return 0;

    BigInt result(0);
    result.resize(dividend.digits.size() - divider.digits.size() + 1);
    
    for (int i = result.digits.size() - 1; i >= 0; i--) {
        for (int delta = BigInt::MOD / 10; delta > 0; delta /= 10) {
            do {
                result.addValueToDigit(i, delta);
            } while (divider * result <= dividend);
            result.addValueToDigit(i, -delta);
        }
    }

    result.setNegative(left.getIsNegative() != right.getIsNegative());
    result.removeLeadingZeros();
    return result;
}

BigInt& BigInt::operator/=(const BigInt& right) {
    return *this = *this / right;
}

BigInt operator%(const BigInt& left, const BigInt& right) {
    BigInt result = left - (left / right) * right;
    if (result.getIsNegative()) 
        result += right;
    return result;
}

BigInt& BigInt::operator%=(const BigInt& right) {
    return *this = *this % right;
}

BigInt operator&(const BigInt& left, const BigInt& right) {
    bool sign = left.getIsNegative() && right.getIsNegative();
    std::vector<char> left_binary = BigIntToBin(left);
    std::vector<char> right_binary = BigIntToBin(right);
    
    std::reverse(left_binary.begin(), left_binary.end());
    std::reverse(right_binary.begin(), right_binary.end());

    while (left_binary.size() < right_binary.size())
        left_binary.push_back(0);
    while (left_binary.size() > right_binary.size())
        right_binary.push_back(0);

    std::vector<char> result_binary;
    for (int i = 0; i < left_binary.size(); i++)
        result_binary.push_back(left_binary[i] == 1 && right_binary[i] == 1 ? 1 : 0);

    BigInt result = binToBigInt(result_binary);
    result.setNegative(sign);
    return result;
}

BigInt& BigInt::operator&=(const BigInt& right) {
    *this = *this & right;
    return *this;
}

BigInt operator|(const BigInt& left, const BigInt& right) {
    bool sign = left.getIsNegative() || right.getIsNegative();
    std::vector<char> left_binary = BigIntToBin(left);
    std::vector<char> right_binary = BigIntToBin(right);
    
    std::reverse(left_binary.begin(), left_binary.end());
    std::reverse(right_binary.begin(), right_binary.end());

    while (left_binary.size() < right_binary.size())
        left_binary.push_back(0);
    while (left_binary.size() > right_binary.size())
        right_binary.push_back(0);

    std::vector<char> result_binary;
    for (int i = 0; i < left_binary.size(); i++) 
        result_binary.push_back(left_binary[i] == 1 || right_binary[i] == 1 ? 1 : 0);

    BigInt result = binToBigInt(result_binary);
    result.setNegative(sign);
    return result;
}

BigInt& BigInt::operator|=(const BigInt& right) {
    *this = *this | right;
    return *this;
}

BigInt operator^(const BigInt& left, const BigInt& right) {
    bool sign = left.getIsNegative() ^ right.getIsNegative();
    std::vector<char> left_binary = BigIntToBin(left);
    std::vector<char> right_binary = BigIntToBin(right);
    
    std::reverse(left_binary.begin(), left_binary.end());
    std::reverse(right_binary.begin(), right_binary.end());

    while (left_binary.size() < right_binary.size())
        left_binary.push_back(0);
    while (left_binary.size() > right_binary.size())
        right_binary.push_back(0);

    std::vector<char> result_binary;
    for (int i = 0; i < left_binary.size(); i++)
        result_binary.push_back(left_binary[i] != right_binary[i] ? 1 : 0);
    
    BigInt result = binToBigInt(result_binary);
    result.setNegative(sign);
    return result;
}

BigInt& BigInt::operator^=(const BigInt& right) {
    *this = *this ^ right;
    return *this;
}

BigInt& BigInt::operator++() {
    *this = *this + BigInt(1);
    return *this;
}

BigInt BigInt::operator++(int) {
    BigInt temp(*this);
    ++*this;
    return temp;
}

BigInt& BigInt::operator--() {
    *this = *this - BigInt(1);
    return *this;
}

BigInt BigInt::operator--(int) {
    BigInt temp(*this);
    --*this;
    return temp;
}

BigInt &BigInt::operator=(const BigInt &right) {
    if (this == &right) {
        return *this;
    }
    this->isNegative = right.getIsNegative();
    this->digits = right.digits;
    return *this;
}

BigInt BigInt::operator~() const {    
    BigInt copy(*this);
    copy.setNegative(false);
    std::vector<char> binary = BigIntToBin(*this);
    std::reverse(binary.begin(), binary.end());
    for (int i = 0; i < binary.size(); i++) {
        binary[i] = !binary[i];
    }

    BigInt result = binToBigInt(binary);
    result.setNegative(!this->getIsNegative());
    return result;
}

BigInt::operator int() const {
    if (*this >= BigInt(INT_MAX))
        return INT_MAX;
    if (*this <= BigInt(INT_MIN + 1))
        return INT_MIN + 1;

    int result = this->digits[0];
    if (this->digits.size() == 2)
        result += this->digits[1] * BigInt::MOD;
    if (this->getIsNegative())
        result = -result;
    return result;
}

BigInt::operator std::string() const {
    std::stringstream ss;
    std::string result;
    ss << *this;
    ss >> result;
    return result;
}

size_t BigInt::size() const {
    return sizeof(std::vector<int>) + (sizeof(int) * this->digits.size()) + sizeof(this->isNegative);
}
