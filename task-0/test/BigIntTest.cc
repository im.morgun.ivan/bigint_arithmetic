#include <BigInt.hpp>
#include <gtest/gtest.h>
#include <climits>


class ConstructorTester {
    public:
        static void TestEmptyConstructor() {
            BigInt bi;
            EXPECT_EQ(bi, 0);
        }

        static void TestStringConstructor(const std::string strValue, bool isCorrect) {
            if (!isCorrect) {
                try {
                    BigInt bi(strValue);
                    FAIL() << "Expected std::invalid_argument";
                } catch (const std::invalid_argument &e) {
                    EXIT_SUCCESS;
                }
            }

            BigInt bi(strValue);
            std::stringstream ss;
            std::string stringFromBigIng;
            ss << bi;
            ss >> stringFromBigIng;
            EXPECT_EQ(strValue, stringFromBigIng);
        }

        static void TestIntConstructor(const int value) {
            BigInt bi(value);
            std::stringstream ssInt, ssBi;
            ssInt << value;
            ssBi << bi;
            std::string intString = ssInt.str();
            std::string biString = ssBi.str();
            EXPECT_EQ(intString, biString);  
        }

        static void TestBigIntConstructor(const BigInt value) {
            BigInt bi(value);
            std::stringstream ss1, ss2;
            ss1 << value;
            ss2 << bi;
            std::string valueString = ss1.str();
            std::string biString = ss2.str();
            EXPECT_EQ(biString, valueString);  
        }
};

TEST(ConstructorTest, EmptyConstructor) {
    ConstructorTester::TestEmptyConstructor();
}

TEST(ConstructorTest, StringConstructor) {
    ConstructorTester::TestStringConstructor("100", true);
    ConstructorTester::TestStringConstructor("87823557023539790379035701350", true);
    ConstructorTester::TestStringConstructor("!100", false);
    ConstructorTester::TestStringConstructor("320.1", false);
}

TEST(ConstructorTest, IntConstructor) {
    ConstructorTester::TestIntConstructor(10);
    ConstructorTester::TestIntConstructor(-10);
    ConstructorTester::TestIntConstructor(1958674883);
    ConstructorTester::TestIntConstructor(-1958674883);
}

TEST(ConstructorTest, BigIntConstructor) {
    ConstructorTester::TestBigIntConstructor(BigInt(10));
    ConstructorTester::TestBigIntConstructor(BigInt(-10));
    ConstructorTester::TestBigIntConstructor(BigInt(1495847362));
    ConstructorTester::TestBigIntConstructor(BigInt(-1495847362));
    ConstructorTester::TestBigIntConstructor(BigInt("23309504768088072020573"));
    ConstructorTester::TestBigIntConstructor(BigInt("-23309504768088072020573"));
}


class ComparisonOperatorTester {
    public:
        static void TestEqualToOperator(BigInt bi1, BigInt bi2, bool equal) {
            EXPECT_EQ(bi1 == bi2, equal);
        }

        static void TestNotEqualToOperator(BigInt bi1, BigInt bi2, bool notEqual) {
            EXPECT_EQ(bi1 != bi2, notEqual);
        }

        static void TestGreaterThanOperator(BigInt bi1, BigInt bi2, bool greater) {
            EXPECT_EQ(bi1 > bi2, greater);
        }

        static void TestLessThanOperator(BigInt bi1, BigInt bi2, bool greater) {
            EXPECT_EQ(bi1 < bi2, greater);
        }

        static void TestGreaterThanOrEqualToOperator(BigInt bi1, BigInt bi2, bool greaterOrEqual) {
            EXPECT_EQ(bi1 >= bi2, greaterOrEqual);
        }

        static void TestLessThanOrEqualToOperator(BigInt bi1, BigInt bi2, bool lessOrEqual) {
            EXPECT_EQ(bi1 <= bi2, lessOrEqual);
        }
};

TEST(ComparisonOperatorTest, EqualToOperator) {
    ComparisonOperatorTester::TestEqualToOperator(BigInt(10), BigInt(10), true);
    ComparisonOperatorTester::TestEqualToOperator(BigInt(-10), BigInt(-10), true);
    ComparisonOperatorTester::TestEqualToOperator(BigInt("10293847699428495893892"), BigInt("10293847699428495893892"), true);
    ComparisonOperatorTester::TestEqualToOperator(BigInt("-10293847699428495893892"), BigInt("-10293847699428495893892"), true);
    ComparisonOperatorTester::TestEqualToOperator(BigInt(10), BigInt(-10), false);
    ComparisonOperatorTester::TestEqualToOperator(BigInt(-10), BigInt(10), false);
    ComparisonOperatorTester::TestEqualToOperator(BigInt("-10293847699428495893892"), BigInt("10293847699428495893892"), false);
    ComparisonOperatorTester::TestEqualToOperator(BigInt("10293847699428495893892"), BigInt("-10293847699428495893892"), false);
    ComparisonOperatorTester::TestEqualToOperator(BigInt(10), BigInt(11), false);
    ComparisonOperatorTester::TestEqualToOperator(BigInt("10293847699428495893892"), BigInt("1029388495893892"), false);
}

TEST(ComparisonOperatorTest, NotEqualToOperator) {
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt(10), BigInt(10), false);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt(-10), BigInt(-10), false);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt("10293847699428495893892"), BigInt("10293847699428495893892"), false);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt("-10293847699428495893892"), BigInt("-10293847699428495893892"), false);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt(10), BigInt(-10), true);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt(-10), BigInt(10), true);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt("-10293847699428495893892"), BigInt("10293847699428495893892"), true);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt("10293847699428495893892"), BigInt("-10293847699428495893892"), true);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt(10), BigInt(11), true);
    ComparisonOperatorTester::TestNotEqualToOperator(BigInt("10293847699428495893892"), BigInt("1029388495893892"), true);
} 

TEST(ComparisonOperatorTest, GreaterThanOperator) {
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(10), BigInt(10), false);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt("330734734081408703740"), BigInt("330734734081408703740"), false);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt("-330734734081408703740"), BigInt("330734734081408703740"), false);   
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt("330734734081408703740"), BigInt("-330734734081408703740"), true);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(20), BigInt(1), true);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(-100), BigInt(-101), true);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(1), BigInt(20), false);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(-101), BigInt(-100), false);
    ComparisonOperatorTester::TestGreaterThanOperator(BigInt(99), BigInt(100), false);
}

TEST(ComparisonOperatorTest, GreaterThanOrEqualToOperator) {
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt("330734734081408703740"), BigInt("-330734734081408703740"), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt(20), BigInt(1), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt(-100), BigInt(-101), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt(10), BigInt(10), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt("330734734081408703740"), BigInt("330734734081408703740"), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt(-10), BigInt(-10), true);
    ComparisonOperatorTester::TestGreaterThanOrEqualToOperator(BigInt("-10293847699428495893892"), BigInt("-10293847699428495893892"), true);
}

TEST(ComparisonOperatorTest, LessThanOperator) {
    ComparisonOperatorTester::TestLessThanOperator(BigInt(10), BigInt(10), false);
    ComparisonOperatorTester::TestLessThanOperator(BigInt("330734734081408703740"), BigInt("330734734081408703740"), false);
    ComparisonOperatorTester::TestLessThanOperator(BigInt("-330734734081408703740"), BigInt("330734734081408703740"), true);   
    ComparisonOperatorTester::TestLessThanOperator(BigInt("330734734081408703740"), BigInt("-330734734081408703740"), false);
    ComparisonOperatorTester::TestLessThanOperator(BigInt(20), BigInt(1), false);
    ComparisonOperatorTester::TestLessThanOperator(BigInt(-100), BigInt(-101), false);
    ComparisonOperatorTester::TestLessThanOperator(BigInt(1), BigInt(20), true);
    ComparisonOperatorTester::TestLessThanOperator(BigInt(-101), BigInt(-100), true);
    ComparisonOperatorTester::TestLessThanOperator(BigInt(99), BigInt(100), true);
}

TEST(ComparisonOperatorTest, LessThanOrEqualToOperator) {
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt("-330734734081408703740"), BigInt("330734734081408703740"), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt(1), BigInt(20), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt(-101), BigInt(-100), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt(99), BigInt(100), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt(10), BigInt(10), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt(-10), BigInt(-10), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt("10293847699428495893892"), BigInt("10293847699428495893892"), true);
    ComparisonOperatorTester::TestLessThanOrEqualToOperator(BigInt("-10293847699428495893892"), BigInt("-10293847699428495893892"), true);
}


class ArithmeticOperatorTester {
    public:
        static void TestAddition(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 + bi2, result);
            bi1 += bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestSubtraction(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 - bi2, result);
            bi1 -= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestMultiplication(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 * bi2, result);
            bi1 *= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestDivision(BigInt bi1, BigInt bi2, BigInt result) {
            if (bi2 == BigInt(0)) {
                try {
                    BigInt testResult = bi1 / bi2;
                    EXPECT_EQ(testResult, result);
                    bi1 /= bi2;
                    EXPECT_EQ(bi1, result);
                    FAIL() << "Expected std::logic_error";
                } catch (const std::logic_error &e) {
                    EXIT_SUCCESS;
                }
            }
            EXPECT_EQ(bi1 / bi2, result);
            bi1 /= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestDivisionWithReminder(BigInt bi1, BigInt bi2, BigInt result) {
            if (bi2 == BigInt(0)) {
                try{
                    EXPECT_EQ(bi1 % bi2, result);
                    bi1 %= bi2;
                    EXPECT_EQ(bi1, result);
                    FAIL() << "Expected std::logic_error";
                } catch (const std::logic_error &e) {
                    EXIT_SUCCESS;
                }
            }
            EXPECT_EQ(bi1 % bi2, result);
            bi1 %= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestPow(BigInt bi, int degree, BigInt result) {
            EXPECT_EQ(pow(bi, degree), result);
        }
};

TEST(ArithmeticOperatorTest, AdditionOperator) {
    ArithmeticOperatorTester::TestAddition(BigInt(10), BigInt(20), BigInt(30));
    ArithmeticOperatorTester::TestAddition(BigInt("4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("4633659436614266825922564284"));
    ArithmeticOperatorTester::TestAddition(BigInt("-4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("-4502205522082411690672032430"));
    ArithmeticOperatorTester::TestAddition(BigInt("4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("4502205522082411690672032430"));
    ArithmeticOperatorTester::TestAddition(BigInt("-4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-4633659436614266825922564284"));
}

TEST(ArithmeticOperatorTest, SubtractionOperator) {
    ArithmeticOperatorTester::TestSubtraction(BigInt(20), BigInt(10), BigInt(10));
    ArithmeticOperatorTester::TestSubtraction(BigInt("4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("4502205522082411690672032430"));
    ArithmeticOperatorTester::TestSubtraction(BigInt("-4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("-4633659436614266825922564284"));
    ArithmeticOperatorTester::TestSubtraction(BigInt("4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("4633659436614266825922564284"));
    ArithmeticOperatorTester::TestSubtraction(BigInt("-4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-4502205522082411690672032430"));
}

TEST(ArithmeticOperatorTest, MultiplicationOperator) {
    ArithmeticOperatorTester::TestMultiplication(BigInt(20), BigInt(10), BigInt(200));
    ArithmeticOperatorTester::TestMultiplication(BigInt("348848223872882828"), BigInt("54590459804"), BigInt("19043784943029403227535845712"));
    ArithmeticOperatorTester::TestMultiplication(BigInt("4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("300236302863770855761072562949442465999860117785181939"));
    ArithmeticOperatorTester::TestMultiplication(BigInt("-4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("-300236302863770855761072562949442465999860117785181939"));
    ArithmeticOperatorTester::TestMultiplication(BigInt("4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-300236302863770855761072562949442465999860117785181939"));
    ArithmeticOperatorTester::TestMultiplication(BigInt("-4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("300236302863770855761072562949442465999860117785181939"));
}

TEST(ArithmeticOperatorTest, DivisionOperator) {
    ArithmeticOperatorTester::TestDivision(BigInt(20), BigInt(10), BigInt(2));
    ArithmeticOperatorTester::TestDivision(BigInt("4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("69"));
    ArithmeticOperatorTester::TestDivision(BigInt("-4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("-69"));
    ArithmeticOperatorTester::TestDivision(BigInt("4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-69"));
    ArithmeticOperatorTester::TestDivision(BigInt("-4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("69"));
    ArithmeticOperatorTester::TestDivision(BigInt("4567932479348339258297298357565965353023456678"), BigInt("5725655777886544346"), BigInt("797800750962094295892789853"));
    ArithmeticOperatorTester::TestDivision(BigInt("10"), BigInt("0"), BigInt(0));
}

TEST(ArithmeticOperatorTest, DivisionWithReminderOperator) {
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt(20), BigInt(11), BigInt(9));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("32772427999337092153949394"));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("-4567932479348339258297298357"), BigInt("65726957265927567625265927"), BigInt("32954529266590475471316533"));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-32954529266590475471316533"));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("-4567932479348339258297298357"), BigInt("-65726957265927567625265927"), BigInt("-32772427999337092153949394"));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("4567932479348339258297298357565965353023456678"), BigInt("5725655777886544346"), BigInt("4746930705680135540"));
    ArithmeticOperatorTester::TestDivisionWithReminder(BigInt("10"), BigInt("0"), BigInt(0));
}

TEST(ArithmeticOperatorTest, Pow) {
    ArithmeticOperatorTester::TestPow(BigInt("547677986785434324125"), 100, BigInt("7120795883489944872337027984930710771129094124779757575684014265378762857500794071257199916540946731659931497414549033475636125580068062973145186461504104017600214753300007255459590986925085880875234157083363715573963018925799504701460159220438602904500945837728755108249329836818374532492097331605594063280792355883141557247142043414860921462776733378716161447407157363896934232462662272518764301805833281357050992303340566151847540197991003659317439255124326424579429900049077612535885728220990751800741776067376691794669349968120951598335085161877908340511429604221339410683355618646767483773162952274018226767585997261077584697450429728419423507072569935590310392276894603729841314468596788465978541080650867417339774288470298078450245304887348289567026248831269147891822271424136083396792989472286264364958533998589307489673354289506339347374306618727291597391557335516916750501689268228446408992497507193993810031691836052173437158438869674972446034211881066073578695002184169729450280257918213195140209538835499709963769038674332058335223332421465466209778871744581997573346823524436300474669316797605559699841687998784943797342452851058354660956133117145143510589813867538802170630912492936279235596499550292351148976257767539945245625271962445404911769832592494640418524303224002415200761486023276168433858136576599306594148501950157144998535460612488396582890034077530678265566390593137691812535705553150995164692877107267199258304247647983145078358298887410060594643810450708649305023801429055857667265853544137485697663819329408527558407751820298866384601768922237338559483060135028552709348333706624373279282904884446767045210741902683437779391021757548747454490090886259220281479955607019194584152734334813288997489187230943941827981159190163215365076451597753024884059647775589193033983035062553673447311017826879316180577240038767607481228324729696145437507896873131507496642825173436717598507177371219090049547341661698216939482573428769465236117867939216360305984558522188921053776664654699621749742517763735643655277629537891925792791880667209625244140625"));
    ArithmeticOperatorTester::TestPow(BigInt(2), 10, BigInt(1024));
}


class BitwiseOperatorTester {
     public:
        static void TestOr(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 | bi2, result);
            bi1 |= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestAnd(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 & bi2, result);
            bi1 &= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestXOR(BigInt bi1, BigInt bi2, BigInt result) {
            EXPECT_EQ(bi1 ^ bi2, result);
            bi1 ^= bi2;
            EXPECT_EQ(bi1, result);
        }

        static void TestInversion(BigInt bi, BigInt result) {
            EXPECT_EQ(~bi, result);
        }
};

TEST(BitwiseOperatorTest, OrOperator) {
    BitwiseOperatorTester::TestOr(BigInt(10), BigInt(1), BigInt(11));
    BitwiseOperatorTester::TestOr(BigInt("54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("82745789289508252739350469283413214"));
    BitwiseOperatorTester::TestOr(BigInt("-54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("-82745789289508252739350469283413214"));
    BitwiseOperatorTester::TestOr(BigInt("54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("-82745789289508252739350469283413214"));
    BitwiseOperatorTester::TestOr(BigInt("-54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("-82745789289508252739350469283413214"));
}

TEST(BitwiseOperatorTest, AndOperator) {
    BitwiseOperatorTester::TestAnd(BigInt(10), BigInt(1), BigInt(0));
    BitwiseOperatorTester::TestAnd(BigInt("54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("54469551002527235108897226948"));
    BitwiseOperatorTester::TestAnd(BigInt("-54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("54469551002527235108897226948"));
    BitwiseOperatorTester::TestAnd(BigInt("54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("54469551002527235108897226948"));
    BitwiseOperatorTester::TestAnd(BigInt("-54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("-54469551002527235108897226948"));
}

TEST(BitwiseOperatorTest, XOROperator) {
    BitwiseOperatorTester::TestXOR(BigInt(10), BigInt(1), BigInt(11));
    BitwiseOperatorTester::TestXOR(BigInt("54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("82745734819957250212115360386186266"));
    BitwiseOperatorTester::TestXOR(BigInt("-54479845798479832343637357782"), BigInt("82745789279213456786753234543282380"), BigInt("-82745734819957250212115360386186266"));
    BitwiseOperatorTester::TestXOR(BigInt("54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("-82745734819957250212115360386186266"));
    BitwiseOperatorTester::TestXOR(BigInt("-54479845798479832343637357782"), BigInt("-82745789279213456786753234543282380"), BigInt("82745734819957250212115360386186266"));
}

TEST(BitwiseOperatorTest, InversionOperator) {
    BitwiseOperatorTester::TestInversion(BigInt(10), BigInt(-5));
    BitwiseOperatorTester::TestInversion(BigInt("54479845798479832343637357782"), BigInt("-24748316715784505249906592553"));
    BitwiseOperatorTester::TestInversion(BigInt("-54479845798479832343637357782"), BigInt("24748316715784505249906592553"));
    BitwiseOperatorTester::TestInversion(BigInt("54479845798479832343637357782"), BigInt("-24748316715784505249906592553"));
    BitwiseOperatorTester::TestInversion(BigInt("-54479845798479832343637357782"), BigInt("24748316715784505249906592553"));
}


class MiscTester {
    public:
        static void TestToIntConverter(BigInt bi, int result) {
            EXPECT_EQ((int) bi, result);
        }

        static void TestToStringConverter(BigInt bi, std::string result) {
            EXPECT_EQ((std::string) bi, result);
        }
};

TEST(MiscTest, ToIntConverter) {
    MiscTester::TestToIntConverter(BigInt(1000000000), 1000000000);
    MiscTester::TestToIntConverter(BigInt(134356678), 134356678);
    MiscTester::TestToIntConverter(BigInt(-1000000000), -1000000000);
    MiscTester::TestToIntConverter(BigInt(-134356678), -134356678);
    MiscTester::TestToIntConverter(BigInt("23456787654321345676654"), INT_MAX);
    MiscTester::TestToIntConverter(BigInt("-23456787654321345676654"), INT_MIN + 1);
}

TEST(MiscTest, ToStringConverter) {
    MiscTester::TestToStringConverter(BigInt("2345678984321345678"), "2345678984321345678");
    MiscTester::TestToStringConverter(BigInt("-2345678984321345678"), "-2345678984321345678");
}

TEST(ExtraTask, EQ) {
    EXPECT_EQ(1, BigInt(3) - 2);
}
